# II. Images

- [II. Images](#ii-images)
  - [1. Images publiques](#1-images-publiques)
  - [2. Construire une image](#2-construire-une-image)

## 1. Images publiques

🌞 **Récupérez des images**

- avec la commande `docker pull`
- récupérez :
  - l'image `python` officielle en version 3.11 (`python:3.11` pour la dernière version)
```bash
[alexy@TP1-Linux-B2 ~]$ docker pull python:3.11
3.11: Pulling from library/python
bc0734b949dc: Already exists
b5de22c0f5cd: Already exists
917ee5330e73: Already exists
b43bd898d5fb: Already exists
7fad4bffde24: Already exists
1f68ce6a3e62: Pull complete
e27d998f416b: Pull complete
fefdcd9854bf: Pull complete
Digest: sha256:4e5e9b05dda9cf699084f20bb1d3463234446387fa0f7a45d90689c48e204c83
Status: Downloaded newer image for python:3.11
docker.io/library/python:3.11
```
  - l'image `mysql` officielle en version 5.7
```bash
[alexy@TP1-Linux-B2 ~]$ docker pull mysql:5.7
5.7: Pulling from library/mysql
20e4dcae4c69: Pull complete
1c56c3d4ce74: Pull complete
e9f03a1c24ce: Pull complete
68c3898c2015: Pull complete
6b95a940e7b6: Pull complete
90986bb8de6e: Pull complete
ae71319cb779: Pull complete
ffc89e9dfd88: Pull complete
43d05e938198: Pull complete
064b2d298fba: Pull complete
df9a4d85569b: Pull complete
Digest: sha256:4bc6bc963e6d8443453676cae56536f4b8156d78bae03c0145cbe47c2aad73bb
Status: Downloaded newer image for mysql:5.7
docker.io/library/mysql:5.7
```
  - l'image `wordpress` officielle en dernière version
    - c'est le tag `:latest` pour récupérer la dernière version
```bash
[alexy@TP1-Linux-B2 ~]$ docker pull wordpress:latest
latest: Pulling from library/wordpress
af107e978371: Already exists
6480d4ad61d2: Pull complete
95f5176ece8b: Pull complete
0ebe7ec824ca: Pull complete
673e01769ec9: Pull complete
74f0c50b3097: Pull complete
1a19a72eb529: Pull complete
50436df89cfb: Pull complete
8b616b90f7e6: Pull complete
df9d2e4043f8: Pull complete
d6236f3e94a1: Pull complete
59fa8b76a6b3: Pull complete
99eb3419cf60: Pull complete
22f5c20b545d: Pull complete
1f0d2c1603d0: Pull complete
4624824acfea: Pull complete
79c3af11cab5: Pull complete
e8d8239610fb: Pull complete
a1ff013e1d94: Pull complete
31076364071c: Pull complete
87728bbad961: Pull complete
Digest: sha256:be7173998a8fa131b132cbf69d3ea0239ff62be006f1ec11895758cf7b1acd9e
Status: Downloaded newer image for wordpress:latest
docker.io/library/wordpress:latest
```
    - si aucun tag n'est précisé, `:latest` est automatiquement ajouté
  - l'image `linuxserver/wikijs` en dernière version
    - ce n'est pas une image officielle car elle est hébergée par l'utilisateur `linuxserver` contrairement aux 3 précédentes
    - on doit donc avoir un moins haut niveau de confiance en cette image
```bash
[alexy@TP1-Linux-B2 ~]$ docker pull linuxserver/wikijs
Using default tag: latest
latest: Pulling from linuxserver/wikijs
8b16ab80b9bd: Pull complete
07a0e16f7be1: Pull complete
145cda5894de: Pull complete
1a16fa4f6192: Pull complete
84d558be1106: Pull complete
4573be43bb06: Pull complete
20b23561c7ea: Pull complete
Digest: sha256:131d247ab257cc3de56232b75917d6f4e24e07c461c9481b0e7072ae8eba3071
Status: Downloaded newer image for linuxserver/wikijs:latest
docker.io/linuxserver/wikijs:latest
```
- listez les images que vous avez sur la machine avec une commande `docker`
    
```bash
[alexy@TP1-Linux-B2 ~]$ docker images
REPOSITORY           TAG       IMAGE ID       CREATED       SIZE
linuxserver/wikijs   latest    869729f6d3c5   5 days ago    441MB
mysql                5.7       5107333e08a8   8 days ago    501MB
python               latest    fc7a60e86bae   13 days ago   1.02GB
wordpress            latest    fd2f5a0c6fba   2 weeks ago   739MB
python               3.11      22140cbb3b0c   2 weeks ago   1.01GB
nginx                latest    d453dd892d93   8 weeks ago   187MB
```
> Quand on tape `docker pull python` par exemple, un certain nombre de choses est implicite dans la commande. Les images, sauf si on précise autre chose, sont téléchargées depuis [le Docker Hub](https://hub.docker.com/). Rendez-vous avec un navigateur sur le Docker Hub pour voir la liste des tags disponibles pour une image donnée. Sachez qu'il existe d'autres répertoires publics d'images comme le Docker Hub, et qu'on peut facilement héberger le nôtre. C'est souvent le cas en entreprise. **On appelle ça un "registre d'images"**.

🌞 **Lancez un conteneur à partir de l'image Python**

- lancez un terminal `bash` ou `sh`
```bash
[alexy@TP1-Linux-B2 ~]$ docker run -it python:3.11 bash
root@5deddcc4f589:/# python
Python 3.11.7 (main, Dec 19 2023, 20:33:49) [GCC 12.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>>
```
- vérifiez que la commande `python` est installée dans la bonne version

> *Sympa d'installer Python dans une version spéficique en une commande non ? Peu importe que Python soit déjà installé sur le système ou pas. Puis on détruit le conteneur si on en a plus besoin.*

## 2. Construire une image

Pour construire une image il faut :

- créer un fichier `Dockerfile`
- exécuter une commande `docker build` pour produire une image à partir du `Dockerfile`


🌞 **Ecrire un Dockerfile pour une image qui héberge une application Python**
```bash
[alexy@TP1-Linux-B2 ~]$ mkdir build-nul
[alexy@TP1-Linux-B2 ~]$ cd build-nul/
[alexy@TP1-Linux-B2 build-nul]$ touch Dockerfile
```
- l'image doit contenir
  - une base debian (un `FROM`)
  - l'installation de Python (un `RUN` qui lance un `apt install`)
    - il faudra forcément `apt update` avant
    - en effet, le conteneur a été allégé au point d'enlever la liste locale des paquets dispos
    - donc nécessaire d'update avant de install quoique ce soit
  - l'installation de la librairie Python `emoji` (un `RUN` qui lance un `pip install`)
  - ajout de l'application (un `COPY`)
  - le lancement de l'application (un `ENTRYPOINT`)
```bash
[alexy@TP1-Linux-B2 python_app_build]$ cat Dockerfile
FROM debian:latest
RUN apt update && apt install -y python3 python3-pip
RUN pip install emoji --break-system-packages
COPY app.py /app.py
ENTRYPOINT ["python3", "/app.py"]
```
- le code de l'application :

```python
import emoji

print(emoji.emojize("Cet exemple d'application est vraiment naze :thumbs_down:"))
```

- pour faire ça, créez un dossier `python_app_build`
  - pas n'importe où, c'est ton Dockerfile, ton caca, c'est dans ton homedir donc `/home/<USER>/python_app_build`
  - dedans, tu mets le code dans un fichier `app.py`
  - tu mets aussi `le Dockerfile` dedans
```bash
[alexy@TP1-Linux-B2 ~]$ mkdir /home/alexy/python_app_build
[alexy@TP1-Linux-B2 ~]$ nano /home/alexy/python_app_build/app.py
[alexy@TP1-Linux-B2 ~]$ cat /home/alexy/python_app_build/app.py
import emoji

print(emoji.emojize("Cet exemple d'application est vraiment naze :thumbs_down:"))
[alexy@TP1-Linux-B2 ~]$ mv build-nul/Dockerfile /home/alexy/python_app_build/
[alexy@TP1-Linux-B2 ~]$ ls /home/alexy/python_app_build/
app.py  Dockerfile
```

> *J'y tiens beaucoup à ça, comprenez que Docker c'est un truc que le user gère. Sauf si vous êtes un admin qui vous en servez pour faire des trucs d'admins, ça reste dans votre `/home`. Les dévs quand vous bosserez avec Windows, vous allez pas stocker vos machins dans `C:/Windows/System32/` si ? Mais plutôt dans `C:/Users/<TON_USER>/TonCaca/` non ? Alors pareil sous Linux please.*

🌞 **Build l'image**

- déplace-toi dans ton répertoire de build `cd python_app_build`
- `docker build . -t python_app:version_de_ouf`
  - le `.` indique le chemin vers le répertoire de build (`.` c'est le dossier actuel)
  - `-t python_app:version_de_ouf` permet de préciser un nom d'image (ou *tag*)
- une fois le build terminé, constater que l'image est dispo avec une commande `docker`
```bash
[alexy@TP1-Linux-B2 python_app_build]$ docker build . -t python_app:version_de_ouf
[+] Building 17.4s (9/9) FINISHED                                                                                                            docker:default
 => [internal] load .dockerignore                                                                                                                      0.0s
 => => transferring context: 2B                                                                                                                        0.0s
 => [internal] load build definition from Dockerfile                                                                                                   0.0s
 => => transferring dockerfile: 270B                                                                                                                   0.0s
 => [internal] load metadata for docker.io/library/debian:latest                                                                                       0.7s
 => [1/4] FROM docker.io/library/debian:latest@sha256:bac353db4cc04bc672b14029964e686cd7bad56fe34b51f432c1a1304b9928da                                 0.0s
 => [internal] load build context                                                                                                                      0.0s
 => => transferring context: 86B                                                                                                                       0.0s
 => CACHED [2/4] RUN apt update && apt install -y python3 python3-pip                                                                                  0.0s
 => [3/4] RUN pip install emoji --break-system-packages                                                                                                2.6s
 => [4/4] COPY app.py /app.py                                                                                                                          0.1s
 => exporting to image                                                                                                                                13.7s
 => => exporting layers                                                                                                                               13.7s
 => => writing image sha256:0d2e066bcc90ceec159a138691d4e6fa65599d3e89c035edd4a8fdeadba23b3c                                                           0.0s
 => => naming to docker.io/library/python_app:version_de_ouf                                                                                           0.0s
[alexy@TP1-Linux-B2 python_app_build]$ cat Dockerfile
FROM debian:latest
RUN apt update && apt install -y python3 python3-pip
RUN pip install emoji --break-system-packages
COPY app.py /app.py
ENTRYPOINT ["python3", "/app.py"]
[alexy@TP1-Linux-B2 python_app_build]$ docker images
REPOSITORY           TAG              IMAGE ID       CREATED              SIZE
python_app           version_de_ouf   0d2e066bcc90   About a minute ago   636MB
linuxserver/wikijs   latest           869729f6d3c5   5 days ago           441MB
mysql                5.7              5107333e08a8   8 days ago           501MB
python               latest           fc7a60e86bae   13 days ago          1.02GB
wordpress            latest           fd2f5a0c6fba   2 weeks ago          739MB
python               3.11             22140cbb3b0c   2 weeks ago          1.01GB
nginx                latest           d453dd892d93   8 weeks ago          187MB
```

🌞 **Lancer l'image**

- lance l'image avec `docker run` :

```bash
docker run python_app:version_de_ouf
```

```bash	
[alexy@TP1-Linux-B2 python_app_build]$ docker run python_app:version_de_ouf
Cet exemple d'application est vraiment naze 👎
```
